﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalproject
{
    public partial class NewPage : System.Web.UI.Page
    {
        private string add_query = "INSERT INTO PAGES (pagetitle,pagecontent) VALUES";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void add_page(object sender, EventArgs e)
        {
            string title = pagetitle.Text.ToString();
            string content = pagecontent.Text.ToString();
            add_query += "('" +
                title + "', '" + content + "')";
            page_insert.InsertCommand = add_query;
            page_insert.Insert();
            Response.Redirect("page1.aspx");
        }
    }
}
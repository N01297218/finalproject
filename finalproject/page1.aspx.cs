﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalproject
{
    public partial class page1 : System.Web.UI.Page
    {
        private string base_query = "SELECT pageid,pagetitle,pagecontent from pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = base_query;
            pages_list.DataSource = Classes_Manual_Bind(pages_select);
            pages_list.DataBind();
        }
        protected DataView Classes_Manual_Bind(SqlDataSource src)
        {

            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                
                row["pagetitle"] =
                    "<a href=\"mypage1.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";
            }
           
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;

        }

    }
}
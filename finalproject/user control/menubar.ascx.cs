﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalproject.user_control
{
    public partial class menubar : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            select_pages.SelectCommand = "select * from pages";
            //refrred from inclass example
            DataView my_view = (DataView)select_pages.Select(DataSourceSelectArguments.Empty);

            string menu_content = "";
            foreach(DataRowView myrow in my_view)
            {
                menu_content += "<a href=\"page1.aspx?pageid="
                    + myrow["pageid"]
                    + "\">"
                    + myrow["pagetitle"]
                    + "</a>";
            }
            menubar_id.InnerHtml = menu_content;
        }
    }
}
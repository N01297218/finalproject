﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="finalproject.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h3 runat="server" id="new_page">Add Page</h3>
    <div>
       <asp:Label ID="ptitle" runat="server" Text="Page Title" CssClass="label label-default"></asp:Label><br />
        <asp:TextBox ID="pagetitle" runat="server"></asp:TextBox><br />
    </div>
    <asp:SqlDataSource runat="server"
        id="page_insert"
       
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
    <div>
       <asp:Label ID="pcontent" runat="server" Text="Page Content" CssClass="label label-default"></asp:Label><br />
        <asp:TextBox ID="pagecontent" runat="server" TextMode="MultiLine" Columns="30" Rows="6"></asp:TextBox><br />
    </div>
    <div><asp:Button ID="Button1" runat="server" Text="Submit" OnClick="add_page" CssClass="btn btn-outline-info"/></div>
</asp:Content>

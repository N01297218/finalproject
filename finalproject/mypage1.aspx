﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="mypage1.aspx.cs" Inherits="finalproject.mypage1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 id="page_title" runat="server"></h3>
    
     <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
    <div id="fetch_content" runat="server">

    </div>
    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_page_btn"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" class="btn btn-danger"/>
    <div id="del_debug" class="querybox" runat="server"></div>
    <a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Lets Edit!</a>
    

</asp:Content>

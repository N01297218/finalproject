﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalproject
{
    public partial class mypage1 : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        private string base_query = "SELECT pageid,pagetitle,pagecontent from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null) page_title.InnerHtml = "No PAGES found.";
            else
            {

                base_query += " WHERE PAGEID = " + pageid;

                pages_select.SelectCommand = base_query;

                DataView classview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);
                string title = classview[0]["pagetitle"].ToString();
                page_title.InnerHtml = title;
                string content = classview[0]["pagecontent"].ToString();
                fetch_content.InnerHtml = content;
            }
        }
        protected void DelPage(object sender, EventArgs e)
        {
          
            string delquery = "DELETE FROM PAGES WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();

            
            string del_query = "DELETE FROM" +
                " PAGES WHERE PAGEID=" + pageid;
            del_debug.InnerHtml += "<br>BUT ALSO<br>" + del_query;
            del_page.DeleteCommand = del_query;
            del_page.Delete();
        }
    }
}
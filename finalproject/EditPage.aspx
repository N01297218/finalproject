﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="finalproject.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h3 runat="server" id="edit_pages">Edit Page</h3>
    <div>
       <asp:Label ID="ptitle" runat="server" Text="Page Title" class="label label-default"></asp:Label><br />
        <asp:TextBox ID="pagetitle" runat="server"></asp:TextBox><br />
    </div>
    <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
    <div>
       <asp:Label ID="pcontent" runat="server" Text="Page Content" class="label label-default"></asp:Label><br />
        <asp:TextBox ID="pagecontent" runat="server" TextMode="MultiLine" columns="30" Rows="6"></asp:TextBox><br />
    </div>
    <div><asp:Button ID="Button1" runat="server" Text="Submit" OnClick="update_content" class="btn btn-outline-info"/></div>
    
</asp:Content>

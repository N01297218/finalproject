﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace finalproject
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void update_content(object sender, EventArgs e)
        {

            string title = pagetitle.Text;
            string content = pagecontent.Text;
            string editquery = "Update pages set pagetitle='" + title + "'," +
                  "pagecontent= '" + content + "'" +
                  " where pageid=" + pageid;
            

            pages_select.UpdateCommand = editquery;
            pages_select.Update();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            DataRowView studentrow = getClassInfo(pageid);
            if (studentrow == null)
            {
                edit_pages.InnerHtml = "No Pages Found.";
                return;
            }
            pagetitle.Text = studentrow["pagetitle"].ToString();
            pagecontent.Text = studentrow["pagecontent"].ToString();

        }
        protected DataRowView getClassInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            pages_select.SelectCommand = query;

            DataView classview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);


            if (classview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView classrowview = classview[0];
            return classrowview;
        }
    }
}
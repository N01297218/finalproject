﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="page1.aspx.cs" Inherits="finalproject.page1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Handle Pages</h2>
    <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:my_database %>">
    </asp:SqlDataSource>
    
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>
    <%--add button--%>
    <a href="NewPage.aspx">Add New PAge Here..!!</a>
</asp:Content>
